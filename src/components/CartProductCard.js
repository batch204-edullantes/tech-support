import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CartProductCard({courseProp}) {

		console.log(courseProp);

		return (
				<Card className="mb-2 col-12 col-md-4 col-xl-3">
						<Card.Body>
								<Card.Title>{courseProp.productName}</Card.Title>
								<Card.Subtitle>courseProp.productDescription:</Card.Subtitle>
								<Card.Text>{courseProp.productDescription}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP {courseProp.price}</Card.Text>

								{/* <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link> */}
						</Card.Body>
				</Card>
	)
}