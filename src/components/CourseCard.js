import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

		const {_id, name, description, price} = courseProp;

		// console.log(courseProp);

		return (
				<Card className="mb-2 col-12 col-md-4 col-xl-3">
						<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP {price}</Card.Text>

								<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
						</Card.Body>
				</Card>
	)
}