import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function CartView(props){

	console.log(props)

	//destructure the coursesProp and the fetchData function from Courses.js
	const { coursesProp, fetchData } = props;

	const [coursesArr, setCoursesArr] = useState([])
	const [courseId, setCourseId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [totalPrice, setTotalPrice] = useState(0);
	//states for handling modal visibility
	const [showEdit, setShowEdit] = useState(false)
	const [showAdd, setShowAdd] = useState(false);

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals
	const openEdit = (courseId) => {
		// console.log(courseId)
		fetch(`https://pure-headland-93549.herokuapp.com/products/${courseId}`, {
			headers: {
				Authorization: `Bearer ${localStorage.token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourseId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShowEdit(true)
	}

	const closeEdit = () => {
		setCourseId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
		setShowAdd(false)
	}

	const addProduct = (e) => {
		e.preventDefault()

		fetch(`https://pure-headland-93549.herokuapp.com/products`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully added!")
				//close the modal and set all states back to default values
				closeEdit()
				fetchData()
				//we call fetchData here to update the data we receive from the database
				//calling fetchData updates our coursesProp, which the useEffect below is monitoring
				//since the courseProp updates, the useEffect runs again, which re-renders our table with the updated data
			}else{
				alert("Something went wrong")
			}
		})
	}

	const editCourse = (e) => {
		e.preventDefault()

		fetch(`https://pure-headland-93549.herokuapp.com/products/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Course successfully updated")
				//close the modal and set all states back to default values
				closeEdit()
				fetchData()
				//we call fetchData here to update the data we receive from the database
				//calling fetchData updates our coursesProp, which the useEffect below is monitoring
				//since the courseProp updates, the useEffect runs again, which re-renders our table with the updated data
			}else{
				alert("Something went wrong")
			}
		})
	}

	const deleteCartProduct = (productName) => {
		fetch(`https://pure-headland-93549.herokuapp.com/users/cart/${productName}`, {
			method: "DELETE",
			headers: {
				Authorization: `Bearer ${token}`
			},
		})
		.then(res => res.json())
		.then(data => {
				alert(`Product has been removed!`)
				fetchData()
		})
	}

	const archiveToggle = (courseId, isActive) => {
		fetch(`https://pure-headland-93549.herokuapp.com/products/${courseId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				alert(`Course successfully ${bool}`)

				fetchData()
			}else{
				alert("Something went wrong")
			}
		})
	}

	useEffect(() => {
		//map through the coursesProp to generate table contents
		const courses = coursesProp.map(course => {
			setTotalPrice(totalPrice + (course.quantity * course.price));
			return(
				<tr key={course.productName}>
					<td>{course.productName}</td>
					<td>{course.productDescription}</td>
					<td>{course.price}</td>
					<td>{course.quantity}</td>
					<td>{course.quantity * course.price}</td>
					<td>
						<Button variant="danger" size="sm" onClick={() => deleteCartProduct(course.productName)}>Remove</Button>
					</td>
				</tr>
			)
		})

	

		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
		setCoursesArr(courses)

	}, [coursesProp])
	
	return(
		<>
		{console.log(totalPrice)}
			<div>
				<h2>Cart Products</h2>
			</div>
			

			{/*Course info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the coursesProp*/}
					{coursesArr}
				</tbody>
			</Table>
		</>
	)
}