import { useState, useEffect } from 'react';
import { Container, Card } from 'react-bootstrap';
import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function SpecificCourse({match}) {

	// console.log(match)

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	//match.params holds the ID of our course in the courseId property
	const courseId = match.params.courseId;

	// console.log(courseId)

	useEffect(() => {
		fetch(`https://pure-headland-93549.herokuapp.com/products/${courseId}`, {
			headers: {
				Authorization: `Bearer ${localStorage.token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	const addToCart = () => {
		fetch(`https://pure-headland-93549.herokuapp.com/users/cart`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.token}`
			},
			body: JSON.stringify({
				productName: name,
				productDescription: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
				alert(`Product has been added to cart`)
		})
	}

	return(
		<Container className="mt-5">
			<Card>
				<Card.Body className="text-center">
					<Card.Subtitle>Name:</Card.Subtitle>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
					<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
					<Form.Control
						type="number"
						placeholder="Enter password"
						value={quantity}
						onChange={e => setQuantity(e.target.value)}
						style={{width: '25%', margin: 'auto', marginBottom: '1rem'}}
						required
					/>
				</Form.Group>
					<Button variant="primary" size="sm" onClick={() => addToCart()}>Add to Cart</Button>
				</Card.Body>
			</Card>
		</Container>
	)
}
