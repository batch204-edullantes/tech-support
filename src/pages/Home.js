import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "TECHNOLOGY SUPPORT",
		content: "Opportunities for everyone, everywhere",
		destination: "/products",
		label: "Get your technology now!"
	}

	return(
		<>
			<Banner dataProp={data}/>
			<Highlights />
		</>
	)
}