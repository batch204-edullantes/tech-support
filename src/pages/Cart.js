import { useEffect, useState, useContext } from 'react';
// import courseData from '../data/courseData';
import CartProductCard from '../components/CartProductCard';
import CartView from '../components/CartView';
import UserContext from '../UserContext';

export default function Courses(){

	const [coursesData, setCoursesData] = useState([]);

	const { user } = useContext(UserContext)

	// console.log(user)

	// Check to see if the mock data wa captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// Props
		// is a shorthand for "property" since components are considered as object in ReactJS.
		// Props is a way to pass data from a parent component to a child component.
		// It is synonymous to the function parameter.

	//Fetch by default always makes a GET request, unless a different one is specified
	//ALWAYS add fetch requests for getting data in a useEffect hook

	const fetchData = () => {
		setTimeout(function() {
				fetch(`https://pure-headland-93549.herokuapp.com/users/cart/products`, {
					headers: {
						Authorization: `Bearer ${localStorage.token}`
					}
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					setCoursesData(data)
				})
		}, 500)
	}

	useEffect(() => {
		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time (when starting the project locally)

		fetchData()
	}, [])

	const courses = coursesData.map(course =>{
		// console.log(course);
			return(
				<CartProductCard courseProp={course} key={course._id} />
			)
	})

	return(
		<CartView coursesProp={coursesData} fetchData={fetchData}/>
	)
}